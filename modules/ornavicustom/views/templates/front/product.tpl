<div id="ornavi_length_select">
    aaaa
</div>
<div id="ornavi_filter">
    filter filter filter
</div>

<div class="ornavi_custom">

    <span class="ornavi_head">
        <span><a href="#">Nápověda</a></span><span><a href="#">Správná velikost zápěstí</a></span><span><a href="#">Sdílejte svůj náramek</a></span><span><a href="#">Uložit a dokončit později</a></span>
    </span>

    <div class="ornavi_info">
        <span class="ornavi_box_right">
            DÉLKA: <span id="ornavi_length">0</span>cm
        </span>

        <span class="ornavi_length_warning">
            Délka vašeho náramku je ideální
        </span>

        <span class="ornavi_box_left">
            CENA: <span id="ornavi_price">0</span>{$currency}
        </span>
    </div>

    <div class="ornavi_string_container">
        <div class="ornavi_string"></div>
    </div>

    <div class="ornavi_filter">
        {foreach from=$filterValues key=filterName item=filter name=filterForeach}
            <div data-filter_name="{$filterName}" class="ornavi_level{$smarty.foreach.filterForeach.iteration}">
                {foreach from=$filter item=filterVal}
                    <span data-filter_value="{$filterVal}" class="ornavi_attr"><span>{$filterVal}</span></span>
                {/foreach}
                {if $smarty.foreach.filterForeach.iteration == 1}
                    <a class="ornavi_filter_modal ornavi_attr" href="#ornavi_filter"><span>Filtr</span></a>
                {/if}
            </div>
        {/foreach}
    </div>
    <i class="arrow_left"></i>
    <div class="ornavi_product_container">
        {foreach from=$products item=product}
            {if $product.quantity < 1}
                {continue}
            {/if}

            <div class="ornavi_product"

            {assign var='size' value=''}

            {foreach from=$product.features item=feature}
                {$feature.name} = "{$feature.value}"
                {if $feature.name == 'ornavi_custom_size'}
                    {$size = $feature.value}
                {/if}
            {/foreach}

            >
                <div class="ornavi_name">{$product.name}</div>

                <img class="img-responsive ornavi_product_img {if $size}ornavi_{$size}{/if}"
                 data-price = "{$product.price_without_reduction}"
                 data-id_product = "{$product.id_product}"
                 data-length = "{$size|substr:0:-2}"
                 src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" 
                 alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" 
                 title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}"
                 itemprop="image" /> 

                 <div class="ornavi_price">
                 Cena: 
                    {convertPrice price=$product.price}
                    {if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
                                            <span class="old-price product-price">
                                                    {displayWtPrice p=$product.price_without_reduction}
                                            </span>
                                            {if $product.specific_prices.reduction_type == 'percentage'}
                                                    <span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
                                            {/if}
                                    {/if}
                 </div>        
            </div>
        {/foreach}
    </div>
    <i class="arrow_right"></i>

    <div class="ornavi_submit">
        <form name="ornavi_custom_to_basket" action="{$link->getModuleLink('ornavicustom', 'product')}" method="POST">
            <input type="hidden" id="ornavi_custom_products" name="ornavi_custom_products" value="">
            <button type="submit" name="submit">Uzel</button>
            <span>TÍMTO KROKEM SE POSUNETE K DOKONČENÍ NÁRAMKU A VAŠÍ OBJEDNÁVKY</span>
        </form>
    </div>
</div>
            
           <a class="various" href="#ornavi_length_select">Inline</a> 
            