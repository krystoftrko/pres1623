<div class="panel">
    <div class="panel-heading">
        <i class="icon-shopping-cart"></i>
        {l s="Ornavi custom" mod="ornavicustom"}
    </div>
    
    {foreach from=$ornaviProducts item=ornaviProduct}
        <div class="ornavi_string"></div>
        <div class="ornavi_string_container">
            {foreach from=$ornaviProduct item=product}
                
                {assign var='size' value=''}

                {foreach from=$product.features item=feature}
                    {if $feature.name == 'ornavi_custom_size'}
                        {$size = $feature.value}
                    {/if}
                {/foreach}
                
                
                <div class="ornavi_on_string">
                    <img class="img-responsive ornavi_product_img {if $size}ornavi_{$size}{/if}"
                        src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" 
                        alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" 
                        title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}"
                        itemprop="image" /> 
                </div>
            {/foreach}
        </div>
        
        <div class="table-responsive">
            <table class="table" id="orderProducts">
                <thead>
                    <tr>
                        {capture "TaxMethod"}
                                {if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
                                        {l s='tax excluded.'}
                                {else}
                                        {l s='tax included.'}
                                {/if}
                        {/capture}
                            <th></th>
                            <th><span class="title_box ">{l s='Product'}</span></th>
       
                            <th>
                                    <span class="title_box ">{l s='Unit Price'}</span>
                                    <small class="text-muted">{l s='tax excluded.'}</small>
                            </th>
                 
                            <th>
                                    <span class="title_box ">{l s='Unit Price'}</span>
                                    <small class="text-muted">{$smarty.capture.TaxMethod}</small>
                            </th>
                            <th class="text-center"><span class="title_box ">{l s='Qty'}</span></th>
                           
                            <th class="text-center"><span class="title_box ">{l s='Available quantity'}</span></th>
                            <th>
                                    <span class="title_box ">{l s='Total'}</span>
                                    <small class="text-muted">{$smarty.capture.TaxMethod}</small>
                            </th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$ornaviProduct item=product}
                        <tr class="product-line-row">
                            <td>
                                <img class="ornavi-thumbnail" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'cart_default')|escape:'html':'UTF-8'}" /> 
                            </td>    
                            <td>
                                <a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$product['id_product']|intval}&amp;updateproduct&amp;token={getAdminToken tab='AdminProducts'}">
                                    <span class="productName">{$product['name']}</span><br />
                                </a>
                            </td>  
                            <td>
                                {displayPrice price=$product.price_tax_exc currency=$currency->id}
                            </td>    
                            <td>
                                {displayPrice price=$product.price currency=$currency->id}
                            </td>   
                            <td class="productQuantity text-center">
                                1
                            </td>
                            <td class="productQuantity text-center">
                                <span class="product_quantity_show{if (int)$product['quantity'] > 1} badge{/if}">{(int)$product['quantity']}</span>
                            </td>   
                            <td>
                                {displayPrice price=$product.price currency=$currency->id}
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
    </div>
        
        

            

    {/foreach}
</div>