<div id="product-ModuleOrnaviCustom" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="ModuleOrnaviCustom" />
	<h3>{l s='Settings' mod='ornavicustom'}</h3>
        
        <label class="control-label col-lg-2">
                {l s='Product is for ornavi custom' mod='ornavicustom'}
        </label>
        <div class="tab-content">
            <div class="input-group col-lg-9 fixed-width-lg">
                    <span class="switch prestashop-switch">
                            <input type="radio" name="for_ornavicustom" id="for_ornavicustom" value="1" {if $value==1}checked="checked"{/if}>
                            <label for="for_ornavicustom" class="radioCheck">
                                    {l s='yes'}
                            </label>
                            <input type="radio" name="for_ornavicustom" id="for_ornavicustom_off" value="0" {if $value==0}checked="checked"{/if}>
                            <label for="for_ornavicustom_off" class="radioCheck">
                                    {l s='No'}
                            </label>
                            <a class="slide-button btn"></a>
                    </span>
            </div>
        </div>

        <div class="panel-footer">
            <button type="submit" name="submitAddproduct" class="btn btn-default pull-right" ><i class="process-icon-save"></i> {l s='Save' mod='mallconnector'}</button>
            <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" ><i class="process-icon-save"></i> {l s='Save and Stay' mod='mallconnector'}</button>
        </div>
</div>
