$(document).ready(function(){
    
    $('.ornavi_filter_modal').fancybox({
        maxWidth	: 500,
        maxHeight	: 200,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
    });
    
    $('#ornavi_length_select').fancybox({
        maxWidth	: 800,
        maxHeight	: 300,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none'
    }).trigger('click');
    
    $('.ornavi_product_container').on('init', styleEdges);
    $('.ornavi_product_container').on('afterChange', styleEdges);

    $('.ornavi_product_container').slick({
        dots: false,
        infinite: true,
        slidesToShow: 7,
        slidesToScroll: 7,
        centerMode: true,
        arrows: true,
        prevArrow: $('.arrow_left'),
        nextArrow: $('.arrow_right'),
        draggable: false,
        variableWidth: false
    }).mousewheel(function(e) {
        e.preventDefault();
        if (e.deltaY < 0) {
            $(this).slick('slickNext');
          }
          else {
            $(this).slick('slickPrev');
          }
    });
    
    $('*[draggable!=true]','.slick-track').unbind('dragstart');
    
    $('.ornavi_string_container').sortable({
        axis: "x",
        items: '.ornavi_on_string',
        cursor: "move",
        tolerance: "intersect",
        distance: 1
    });

    $('.ornavi_string_container').disableSelection();

    $('.ornavi_product_img').draggable({
        revert: "invalid",
        stack: ".draggable",
        helper: 'clone',
        cursor: "move",
        cursorAt: { top: 30, left: 30 }
    });
    
    $('.ornavi_string_container').droppable({
        accept: ".ornavi_product_img",
        drop: function(event, ui) {
          var droppable = $(this);
          var draggable = ui.draggable;

          if(!draggable.hasClass('ornavi_on_string')){
              
              var newElem = draggable.clone();
              
              newElem.appendTo(droppable);
              newElem.wrap('<div class="ornavi_on_string"></div>');
              $('<div class="ornavi_on_string_delete">✖</div>').insertAfter(newElem)

              updateStats();
              $('.ornavi_string_container').sortable('refresh'); 
          }
        }
    });
    
    $('.ornavi_on_string_delete').live('click',function(){
        $(this).parent().remove();
        updateStats();
    });

    $('form[name="ornavi_custom_to_basket"]').submit(function(){
        var input = $(this).find('input[name="ornavi_custom_products"]');
        var products = getProducts();
        if(products){
            input.val(products);
        }
    });

    $('.ornavi_attr').click(function(){

        var filterName = $(this).parent().attr('data-filter_name');
        var filterValue = $(this).data('filter_value');
        
        if(!filterName || !filterValue){
            return;
        }
        
        $(this).siblings().children().removeClass('ornavi_box_center');
        
        var hash = location.hash.split('-');

        if(hash[0] !== '#ornavi_filter'){
            hash[0] = '#ornavi_filter';
        }

        if($(this).children().hasClass('ornavi_box_center')){
            $(this).children().removeClass('ornavi_box_center')
            hash = removeHashFilter(hash, filterName);
        }
        else{
            $(this).children().addClass('ornavi_box_center');
            hash = addHashFilter(hash, filterName, filterValue);
        }
        
        location.hash = hash.join('-');
        filter();
    });

    initHashFilter();
});

function getProducts(){
    var products = '';

    $('.ornavi_string_container').children('.ornavi_on_string').each(function(){
        var product = $(this).children('img').data('id_product');
        if(!product){
            return false;
        }
        products += product + ' ';
    });

    return products;
}

function updateStats(){
    var price = 0;
    var length = 0;

    $('.ornavi_string_container').children('.ornavi_on_string').each(function(){
        price += $(this).children('img').data('price');
        length += $(this).children('img').data('length');
    });

    $('#ornavi_price').html(Math.round(price * 100) / 100);
    $('#ornavi_length').html(length/10);
}

function filter(){
    var hash = location.hash.split('-');

    if(hash[0] !== '#ornavi_filter'){
        return;
    }
    var slickContainer =  $('.ornavi_product_container');

    slickContainer.slick('slickUnfilter');
    slickContainer.slick('slickFilter', function()
    {
        var allow = true;
        for(var iter = 1; iter < hash.length; iter++){
            var nv = hash[iter].split('=');
            var filterName = decodeURI(nv[0]);
            var filterValue = decodeURI(nv[1]);

            allow &= $(this).attr(filterName) == filterValue;
        }

        return  allow;
    });
    oldSlickStyleDelete();

    
    var slick = slickContainer.slick('getSlick');

    if(slick.slideCount < 7){
        slickContainer.slick('slickSetOption', 'slidesToShow', slick.slideCount, true);
        slickContainer.slick('slickSetOption', 'slidesToScrol', slick.slideCount, true);
    }
    else{
        slickContainer.slick('slickSetOption', 'slidesToShow', 7, true);
        slickContainer.slick('slickSetOption', 'slidesToScrol', 7, true);
    }

    switch(slick.slideCount){
        case 1:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 20, left: -425 });
            break;
        case 2:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 15, left: -160 });
            break;
        case 3:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 15, left: -70 });
            break;
        case 4:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 15, left: -30 });
            break;
        default:
            $('.ornavi_product_img').draggable('option','cursorAt', { top: 20, left: 20 });
    }
}

function removeHashFilter(hash, filterName){
    for(var iter = 1; iter < hash.length; iter++){
        var nv = hash[iter].split('=');

        if(nv[0] === filterName){
            hash.splice(iter, 1);
        }
    }

    return hash;
}

function addHashFilter(hash, filterName, filterValue){
    var addItem = true;

    for(var iter = 1; iter < hash.length; iter++){
        var nv = hash[iter].split('=');

        if(nv[0] === filterName){
            nv[1] = filterValue;
            addItem = false;
        }
        hash[iter] = nv.join('=');
    }

    if(addItem){
        hash[hash.length] = filterName+'='+filterValue
    } 

    return hash;
}

function initHashFilter(){
    var hash = location.hash.split('-');

    if(hash[0] == '#ornavi_filter'){
        for(var iter = 1; iter < hash.length; iter++){
            var nv = hash[iter].split('=');
            var filterLevel = $('div[data-filter_name="'+decodeURI(nv[0])+'"]');
            var filterElem = filterLevel.children('span[data-filter_value="'+decodeURI(nv[1]+'"]'));
            filterElem.children().addClass('ornavi_box_center');
        }
    }

    filter();
}

function styleEdges(){
    oldSlickStyleDelete();
    newSlickStyleAdd();
};

function newSlickStyleAdd(){
    var first = $('.slick-active').first().prev();
    var last = $('.slick-active').last().next();
    first.addClass('ornavi_slick_first');
    last.addClass('ornavi_slick_last');
    setWidthCss(first);
    setWidthCss(last);
}

function oldSlickStyleDelete(){
    var oldFirst = $('.ornavi_slick_first');
    var oldLast = $('.ornavi_slick_last');
    setAutoCss(oldFirst);
    setAutoCss(oldLast);
    oldFirst.removeClass('ornavi_slick_first');
    oldLast.removeClass('ornavi_slick_last');
}

function setAutoCss(elem){
    elem.css('max-width', 'auto');
    elem.css('margin-top', 'auto')
}

function setWidthCss(elem){
    var width = elem.css('width').replace(/[^-\d\.]/g, '');
    var minus = Math.ceil(width/10);
    var finalWidth = width - minus;
    elem.css('max-width', finalWidth+'px');
    elem.css('margin-top', (minus/2)+'px');
}