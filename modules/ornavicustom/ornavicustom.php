<?php
/* 
TODO:
    Ornavi custom product is orderable from list - main combination
    PS_IMAGE_QUALITY - png for porducts images
    BUG - add without cart created
  */
if (!defined('_PS_VERSION_'))
{
    exit;
}

require_once __DIR__.'/classes/OrnaviCustomDb.php';
require_once __DIR__.'/classes/OrnaviCustomProduct.php';
require_once __DIR__.'/classes/OrnaviCustomConfig.php';

class OrnaviCustom extends OrnaviCustomConfig
{
    /** @var OrnaviCustomDb */
    public $db;
    
    /** @var OrnaviCustomProduct */
    public $ornaviCustomProduct;
    
    public function __construct()
    {
        $this->name = 'ornavicustom';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'WeboServis, s.r.o.';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Ornavi custom');
        $this->description = $this->l('Adds special custom product.');
        $this->ps_versions_compliancy = ['min' => '1.6', 'max' => _PS_VERSION_];
            
        $this->db = new OrnaviCustomDb($this->context->language->id, $this->context->shop->id);
        $this->ornaviCustomProduct = new OrnaviCustomProduct($this->db);
    }

    public function install()
    {
        return parent::install()
                & $this->registerHook('displayProductTabContent')
                & $this->registerHook('displayAdminProductsExtra')
                & $this->registerHook('actionProductSave')
                & $this->registerHook('displayBackOfficeHeader')
                & $this->registerHook('displayAdminOrder')
                & $this->registerHook('actionValidateOrder')
                & $this->db->install()
                & $this->installProduct()                                   //Needs old conf value
                & $this->configuration(self::CONFIG_DELETE_DB, true)
                & $this->configuration(self::CONFIG_DELETE_PRODUCTS, true)
                & Configuration::updateValue('PS_IMAGE_QUALITY', 'png');
    }
    

    private function installProduct()
    {
        $deleteProduct = $this->configuration(self::CONFIG_DELETE_PRODUCTS);
        
        if($deleteProduct){
            $productId = $this->ornaviCustomProduct->install();
            $this->configuration(self::CONFIG_FEATURES_ID, $this->ornaviCustomProduct->installFeature());
            return $this->configuration(self::CONFIG_PRODUCT_ID, $productId);
        }
        
        $productId = $this->configuration(self::CONFIG_PRODUCT_ID);
        $product = new Product($productId);
        
        if(!$product->id){
            $this->configuration(self::CONFIG_FEATURES_ID, $this->ornaviCustomProduct->installFeature());
            $productId = $this->ornaviCustomProduct->install();
        }      
        
        return $this->configuration(self::CONFIG_PRODUCT_ID, $productId);
    }
    
    public function uninstall()
    {
        return parent::uninstall()
                & $this->uninstallProduct() //MUST be first because of DB
                & $this->db->uninstall($this->configuration(self::CONFIG_DELETE_DB));
    }
    
    private function uninstallProduct()
    {
        $deleteProducts = $this->configuration(self::CONFIG_DELETE_PRODUCTS);

        if($deleteProducts){
            $result = true;
            $result &= $this->ornaviCustomProduct->uninstall($this->configuration(self::CONFIG_PRODUCT_ID));
            $result &= $this->configuration(self::CONFIG_PRODUCT_ID, null);
            $result &= $this->ornaviCustomProduct->uninstallFeature($this->configuration(self::CONFIG_FEATURES_ID));
            return $result;
        }
        
        return true;
    }
    
    public function hookDisplayProductTabContent($params)
    {
        if($params['product']->id == $this->configuration(self::CONFIG_PRODUCT_ID)){
            Tools::redirect($this->context->link->getModuleLink($this->name, 'product'));
        }
    }
    
    public function hookDisplayAdminProductsExtra()
    {
        $id = Tools::getValue('id_product');
        
        $value = 0;
        
        if($id){
            $value = $this->db->isOrnaviCustom($id);
        }
        
        $this->smarty->assign([
            'value' => $value
        ]);
        return $this->display(__FILE__, 'views/templates/hook/adminProductsExtra.tpl');
    }
    
    public function hookActionProductSave($params)
    {
        $forOrnaviCustom = Tools::getValue('for_ornavicustom');
        if(($forOrnaviCustom === '1' || $forOrnaviCustom === '0') && $params['id_product'] != $this->configuration(self::CONFIG_PRODUCT_ID)){
            $this->db->switchOrnaviCustomProduct($forOrnaviCustom, $params['id_product']);
        }
    }
    
    public function hookDisplayBackOfficeHeader(){
        $this->context->controller->addCSS($this->_path.'views/css/ornavicustomadmin.css');
    }
    
    public function hookDisplayAdminOrder($params)
    {
        $order = new Order($params['id_order']);

        $ornaviProducts = $this->db->getProductsInOrder($order);

        $this->smarty->assign([
            'ornaviProducts' => $ornaviProducts,
            'order' => $order,
            'currency' => $this->context->currency
        ]);
        
        return $this->display(__FILE__, 'views/templates/hook/adminOrder.tpl');
    }
    
    public function hookActionValidateOrder($params)
    {
        $products = $params['order']->getProducts();
        
        foreach($products as $product){
            if($product['product_id'] == $this->configuration(self::CONFIG_PRODUCT_ID)){
                $productsOrnavi = $this->db->getOrnaviIds($product['product_attribute_id']);
                
                foreach ($productsOrnavi as $productOrnavi){
                    StockAvailable::updateQuantity($productOrnavi, null, -1);
                }
            }
        }
    }
          
}
