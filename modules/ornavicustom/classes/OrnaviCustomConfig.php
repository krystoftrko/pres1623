<?php

class OrnaviCustomConfig extends Module{
    
    const CONFIG_DELETE_DB = 'delete_db',
          CONFIG_DELETE_PRODUCTS = 'delete_products',
          CONFIG_PRODUCT_ID = 'product_id',
          CONFIG_FEATURES_ID = 'features_id';
    
    public static $filterValues = [
        'ornavi_custom_type' => [
            'Korálky', 'Mezikorálky', 'Středy'
        ],
        'ornavi_custom_size' => [
            '6mm', '8mm', '10mm'
        ]
    ];

    public function getContent()
    {
        $output = '';

        $url = $this->context->link->getModuleLink($this->name, 'product');
        
        $output .= $this->displayConfirmation($this->l('Customizer link: ').'<a href="'.$url.'">'.$url.'</a>');

        if(Tools::isSubmit('submit'.$this->name))
        {
            $deleteDatabase = Tools::getValue(self::CONFIG_DELETE_DB);
            $deleteProducts = Tools::getValue(self::CONFIG_DELETE_PRODUCTS);
            
            if (($deleteDatabase === '1' || $deleteDatabase === '0') && 
                ($deleteProducts === '1' || $deleteProducts === '0'))
            {
                $this->configuration(self::CONFIG_DELETE_DB, $deleteDatabase);
                $this->configuration(self::CONFIG_DELETE_PRODUCTS, $deleteProducts);
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
            else
            {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            }
        }

        return $output.$this->displayForm();
    }
    
    public function displayForm()
    {
        $fields_form = [];

        $fields_form[0]['form'] = [
            'legend' => [
                'title' => $this->l('Settings'),
            ],
            'input' => [
                [
                    'type' => 'switch',
                    'label' => $this->l('Delete database with uninstall'),
                    'name' => self::CONFIG_DELETE_DB,
                    'values' =>[
                        [
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ],
                        [
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No') 
                        ]
                    ]
                ],
                [
                    'type' => 'switch',
                    'label' => $this->l('Delete products with uninstall'),
                    'name' => self::CONFIG_DELETE_PRODUCTS,
                    'values' =>[
                        [
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ],
                        [
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No') 
                        ]
                    ]
                ]
            ],
            'submit' => [
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            ]
        ];

        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        
        $helper->fields_value[self::CONFIG_DELETE_DB] = $this->configuration(self::CONFIG_DELETE_DB);
        $helper->fields_value[self::CONFIG_DELETE_PRODUCTS] = $this->configuration(self::CONFIG_DELETE_PRODUCTS);
       
        return $helper->generateForm($fields_form);
    }
    
    public function configuration($conf, $set = null)
    {
        if(is_null($set))
        {
            return Configuration::get($this->name.'_'.$conf);
        }
        else
        {
            return Configuration::updateValue($this->name.'_'.$conf, $set); 
        }
    }
}
