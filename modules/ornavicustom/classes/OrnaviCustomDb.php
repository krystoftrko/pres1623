<?php

class OrnaviCustomDb
{
    const PRODUCT = 'ornavicustom_products',
          ATTRIBUTE = 'ornavicustom_attribute',
          PS_PRODUCT = 'product',
          PS_PRODUCT_SHOP = 'product_shop',
          PS_PRODUCT_LANG = 'product_lang',
          PS_IMAGE_SHOP = 'image_shop';
    
    private $db;
    
    private $langId;
    private $shopId;
    
    public function __construct($lang, $shop)
    {
        $this->db = Db::getInstance();
        $this->langId = $lang;
        $this->shopId = $shop;
    }
    
    public function install()
    {
        $result = true;
        
        $result &= $this->db->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::PRODUCT.'` (
              `id_product` int NOT NULL
            );
                ');
        
        $result &= $this->db->execute('
            CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.self::ATTRIBUTE.'` (
                `id_product_attribute` int NOT NULL,
                `id_product` int NOT NULL
            );
                ');
        
        return $result;
    }
    
    public function uninstall($delete)
    {
        if(!$delete){
            return true;
        }

        $result = true;
        
        $result &= $this->db->execute('
            DROP TABLE IF EXISTS `'._DB_PREFIX_.self::PRODUCT.'`;
                ');
        
        $result &= $this->db->execute('
            DROP TABLE IF EXISTS `'._DB_PREFIX_.self::ATTRIBUTE.'`;
                ');
        
        return $result;
    }
    
    public function isOrnaviCustom($id)
    {
        $q = (new DbQuery())
                ->select('count(*)')
                ->from(self::PRODUCT)
                ->where('id_product = '. pSQL($id));

        $res = $this->db->getRow($q);
        return (bool)$res['count(*)'];
    }
    
    public function switchOrnaviCustomProduct($value, $id)
    {
        if($value){
            $result = true;
            
            $data = [
                'visibility' => 'none',
                'indexed' => 0,
                'available_for_order' => 0,
                'show_price' => 0,
            ];
            
            $where = 'id_product = '.pSQL($id);
            
            $result &= $this->db->update(self::PS_PRODUCT, $data, $where);
            $result &= $this->db->update(self::PS_PRODUCT_SHOP, $data, $where);
            
            if($this->isOrnaviCustom($id)){
                $result &= $this->db->delete(self::PRODUCT, 'id_product = '.pSQL($id));
            }
            $result &= $this->db->insert(self::PRODUCT, ['id_product' => $id]);
            return $result;
        }
        return $this->db->delete(self::PRODUCT, 'id_product = '.pSQL($id));
    }
    
    public function getProductIds()
    {
        $q = (new DbQuery())
                ->select('id_product')
                ->from(self::PRODUCT);
        
        $result = $this->db->executeS($q);
        
        if(!$result){
            return [];
        }
        
        return array_map(function($item){
           return $item['id_product'];
        }, $result);
    }
    
    public function getProducts()
    {
        $q = (new DbQuery())
                ->select('pr.*, pl.*, image_shop.id_image')
                ->from(self::PRODUCT, 'p')
                ->leftJoin(self::PS_PRODUCT_LANG, 'pl', 'pl.id_product = p.id_product AND pl.id_lang = '. pSQL($this->langId).' AND pl.id_shop = '.$this->shopId)
                ->leftJoin(self::PS_PRODUCT, 'pr', 'pr.id_product = p.id_product')
                ->leftJoin(self::PS_IMAGE_SHOP, 'image_shop', 'image_shop.id_product = p.id_product AND image_shop.cover=1 AND pl.id_shop = '.$this->shopId);
        
        $result = $this->db->executeS($q);
        return Product::getProductsProperties($this->langId, $result);
    }
    
    public function getProductsInOrder($order)
    {
        $orderProducts = $order->getProductsDetail();
        
        $ornaviproducts = [];
        foreach ($orderProducts as $ornaviProduct){
            $p = $this->getOrnaviIds($ornaviProduct['product_attribute_id']);
            if($p){
                $ornaviproducts[$ornaviProduct['product_attribute_id']] = implode(',',$p);
            }
        }
        
        $finalProducts = [];
        
        
        foreach ($ornaviproducts as $key=>$ornaviProduct){
            $q = (new DbQuery())
                    ->select('pr.*, pl.*, image_shop.id_image')
                    ->from(self::PS_PRODUCT, 'pr')
                    ->where('pr.id_product IN('.$ornaviProduct.')')
                    ->leftJoin(self::PS_PRODUCT_LANG, 'pl', 'pl.id_product = pr.id_product AND pl.id_lang = '. pSQL($this->langId).' AND pl.id_shop = '.$this->shopId)
                    ->leftJoin(self::PS_IMAGE_SHOP, 'image_shop', 'image_shop.id_product = pr.id_product AND image_shop.cover=1 AND pl.id_shop = '.$this->shopId);
            
            $result = $this->db->executeS($q);
            $finalProducts[$key] = Product::getProductsProperties($this->langId, $result);
        }

        return $finalProducts;
    }
    
    public function addAttribute($attributeId, $ornaviProducts)
    {
        foreach($ornaviProducts as $ornaviProduct){
            $this->db->insert(self::ATTRIBUTE, [
                'id_product_attribute' => $attributeId,
                'id_product' => $ornaviProduct->id
            ]);
        }
    }
    
    public function getOrnaviIds($attributeId)
    {
        $q = (new DbQuery())
                ->select('id_product')
                ->from(self::ATTRIBUTE)
                ->where('id_product_attribute = '.pSQL($attributeId));
        
        $result = $this->db->executeS($q);

        if(!$result){
            return [];
        }
        
        return array_map(function($item){
           return $item['id_product'];
        }, $result);
    }
}
