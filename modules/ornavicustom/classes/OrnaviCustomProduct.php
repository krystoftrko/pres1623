<?php

class OrnaviCustomProduct
{
    /** @var $db OrnaviCustomDb */
    private $db;
    
    public function __construct(OrnaviCustomDb $db) {
        $this->db = $db;
    }
    
    public function install()
    {
        $product = new Product();
        
        foreach (Language::getIDs() as $id){
            $product->name = [$id => 'Ornavi product'];
            $product->link_rewrite = [$id => 'ornavi_product'];
        }
        
        $product->quantity = 0;
        $product->price = 0;
        $product->show_price = 0;
        $product->available_for_order = 1;
        
        $product->add();
        $categories = [];
        
        foreach(Category::getRootCategories() as $cat)
        {
            $categories[] = $cat['id_category'];
        }
        
        $product->addToCategories($categories);
        StockAvailable::setQuantity($product->id, null, 0);
        
        return $product->id;
    }
    
    public function uninstall($id)
    {
        $result = true;
        
        $product = new Product($id);
        $result &= $product->delete();
        
        foreach ($this->db->getProductIds() as $productId)
        {
            $product = new Product($productId);
            $result &= $product->delete();
        }
        
        return $result;
    }
    
    public function installFeature()
    {
        $featureIds = [];
        $langIds = Language::getIDs();

        foreach(OrnaviCustomConfig::$filterValues as $featureName => $featureValues){
            $feature = new Feature();
            foreach ($langIds as $id){
                $feature->name = [$id => $featureName];
            }
            $feature->add();
            $featureIds[] = $feature->id;

            foreach($featureValues as $value){
                $featureValue = new FeatureValue();
                foreach ($langIds as $id){
                    $featureValue->value = [$id => $value];
                }
                $featureValue->id_feature = $feature->id;
                $featureValue->add();
            }
        }

        return implode('_', $featureIds);
    }

    public function uninstallFeature($idstring)
    {
        $ids = explode('_', $idstring);
        $result = true;
        
        foreach($ids as $id){
            $feature = new Feature($id);
            $result &= $feature->delete();
        }
        
        return $result;
    }

    public function addNewCombination($mainProductId, $productsString)
    {
        $mainProduct = new Product($mainProductId);
        
        $productIds = explode(' ', $productsString);  
        $products = [];
        
        foreach($productIds as $productId){
            $product = new Product($productId);
            if($product->id){
                $products[] = $product;
            }
        }
        
        $d = $this->countProductAttributes($products);
        
        $attributeId = $mainProduct->addCombinationEntity($d['wholesale_price'], $d['price'], $d['weight'], $d['unit_impact'], $d['ecotax'], $d['quantity'], $d['id_images'], $d['reference'], $d['id_supplier'], $d['ean13'], $d['default']);
        StockAvailable::setQuantity($mainProduct->id, $attributeId, 1);
        
        $this->db->addAttribute($attributeId, $products);
        
        return $attributeId;
    }
    
    private function countProductAttributes($products)
    {
        $data = [
            'wholesale_price' => 0,
            'price' => 0, 
            'weight' => 0, 
            'unit_impact' => 0, 
            'ecotax' => 0, 
            'quantity' => 1, 
            'id_images' => [], 
            'reference' => '',
            'id_supplier' => '', 
            'ean13' => '', 
            'default' => ''
        ];
       
        foreach ($products as $product){
            $data['wholesale_price'] += $product->wholesale_price;
            $data['price'] += $product->price;
            $data['weight'] += $product->weight;
            $data['ecotax'] += $product->ecotax;
            $data['id_images'][] = $product->getCoverWs();
        }
        
        return $data;
    }
}
