<?php

class OrnaviCustomProductModuleFrontController extends ModuleFrontController
{
    /** @var OrnaviCustomDb */
    public $db;
    
    /** @var OrnaviCustomProduct */
    public $product;
    
    public function __construct() {
        parent::__construct();
        $this->path = null;
        $this->display_column_left = false;
        $this->display_column_right = false;
        $this->db = $this->module->db;
        $this->product = $this->module->ornaviCustomProduct;
    }
    
    public function initContent()
    {
        parent::initContent();

        $this->context->smarty->assign([
            'products' => $this->db->getProducts(),
            'link' => $this->context->link,
            'filterValues' => OrnaviCustomConfig::$filterValues,
            'currency' => $this->context->currency->sign
            ]);
        
        $this->setTemplate('product.tpl');
    }
    
    public function setMedia()
    {
        $modulePath = _MODULE_DIR_.$this->module->name.'/views/';
        
        $this->addCSS($modulePath.'css/ornavicustomfront.css');
        $this->addCSS($modulePath.'modules/slick/slick.css');
        $this->addCSS($modulePath.'modules/slick/slick-theme.css');
        
        $this->addJquery();
        $this->addJqueryUI('ui.draggable');
        $this->addJqueryUI('ui.droppable');
        $this->addJqueryUI('ui.sortable');
        $this->addjqueryPlugin('fancybox');
        $this->addJS($modulePath.'modules/slick/slick.min.js');
        $this->addJS($modulePath.'modules/jquery.mousewheel.min.js');
        $this->addJS($modulePath.'js/ornavicustomfront.js');
        
        return parent::setMedia();
    }
    
    public function postProcess() {
        
        $products = Tools::getValue('ornavi_custom_products');
        
        if($products){
            $mainProductId = $this->module->configuration(OrnaviCustom::CONFIG_PRODUCT_ID);
            $combinationId = $this->product->addNewCombination($mainProductId, $products);
            $this->context->cart->updateQty(1, $mainProductId, $combinationId);
        }
        
    }
}
